﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SERFOR.Component.DTEntities.Plantaciones
{
    [DataContract]
    public class PlantacionesFeatureLayer
    {
       
       [DataMember]
        public int ZONUTM { get; set; }
        [DataMember]
        public string NOMREL { get; set; }
        [DataMember]
        public string NOMTIT { get; set; }
        [DataMember]
        public string NOMCCN { get; set; }
        
        [DataMember]
        public string NUMRUC { get; set; }
        [DataMember]
        public byte TIPDOC { get; set; }
        [DataMember]
        public string NRODOC { get; set; }
        [DataMember]
        public string NOMDIS { get; set; }
        [DataMember]
        public string NOMPRO { get; set; }
        [DataMember]
        public string NOMDEP { get; set; }
        [DataMember]
        public int AUTFOR { get; set; }
        [DataMember]
        public decimal? FINALI { get; set; }
        [DataMember]
        public string ESPECI { get; set; }
        [DataMember]
        public decimal? SUPSIG { get; set; }
        [DataMember]
        public decimal? SUPAPR { get; set; }
        [DataMember]
        public double? COORES { get; set; }
        [DataMember]
        public double? COORNO { get; set; }
        [DataMember]
        public string FECPLA { get; set; }
        [DataMember]
        public string FUENTE { get; set; }
        [DataMember]
        public string DOCREG { get; set; }
        [DataMember]
        public string FECREG { get; set; }
        [DataMember]
        public string OBSERV { get; set; }
        [DataMember]
       // public string ORIGEN { get; set; }
       // [DataMember]
        public string DOCASA { get; set; }
        [DataMember]
        public string DOCTIT { get; set; }
        [DataMember]
        public string DOCAUT { get; set; }
        [DataMember]
        public string CONTRA { get; set; }
        [DataMember]
        public string NUMREG { get; set; }
        
    }
}
